package co.velloso.android.textfield

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.SearchView
import com.jakewharton.rxbinding3.appcompat.queryTextChangeEvents
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit


open class DebounceSearchView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = R.attr.searchViewStyle

) : SearchView(context, attrs, defStyleAttr) {

    var maxDebounce: Int = 2

    var timeDebounce: Long = 400L

    private var disposable: Disposable? = null

    private var debounceCounter = 0

    private var listener: SearchDebounceViewListener? = null

    var isQueryFocused = false
        private set

    interface SearchDebounceViewListener {
        fun onDebouncedQuery(view: View, queryText: CharSequence, isSubmitted: Boolean)
    }

    init {
        // Custom attributes
        val a = context.obtainStyledAttributes(attrs, R.styleable.DebounceSearchView, defStyleAttr, 0)

        maxDebounce = a.getInteger(R.styleable.DebounceSearchView_maxDebounce, 2)
        timeDebounce = a.getInteger(R.styleable.DebounceSearchView_timeDebounce, 400).toLong()

        a.recycle()
    }

    fun hasQueryFocus() = isQueryFocused

    override fun setOnQueryTextFocusChangeListener(listener: OnFocusChangeListener?) {
        super.setOnQueryTextFocusChangeListener { v, hasFocus ->
            isQueryFocused = hasFocus
            listener?.onFocusChange(v, hasFocus)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        disposable = queryTextChangeEvents()
                .filter { queryEvent ->
                    debounceCounter += 1
                    // If we are cleaning the view or we already bounce MAX_DEBOUNCE times call the
                    // update immediately
                    if (queryEvent.isSubmitted || queryEvent.queryText.isEmpty() || debounceCounter > maxDebounce) {
                        listener?.onDebouncedQuery(this, queryEvent.queryText.toString(), queryEvent.isSubmitted)
                        debounceCounter = 0
                        false
                    } else true
                }
                .debounce(timeDebounce, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe { queryEvent ->
                    // Debounce do not cancel a queued event if the previous filter return false,
                    // since it only cancel a queued event if .debounce() is called again. Example:
                    // Call is made to query "c" term and it enters the queue. If the user search
                    // another term "ca", .debounce() will be called again, removing "c" from the
                    // queue, replacing it with "ca" and resetting the timer. But if instead the user
                    // backspace the previous "c", the filter() function will return false, debounce
                    // will not be called and "c" will not leave the queue. So to avoid a second
                    // call to the updateProductResults we need a second check. We use the
                    // debounceCounter > 0 because every time the updateProductResults is called we set
                    // debounceCounter to zero.
                    if (debounceCounter > 0) {
                        // Update views that need result data with debounce delay
                        listener?.onDebouncedQuery(this, queryEvent.queryText, queryEvent.isSubmitted)
                        debounceCounter = 0
                    }
                }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        disposable?.dispose()
    }

    /**
     * Sets a query string in the text field and optionally submits the query as well.
     *
     * @param submit if true, immediately call onDebouncedQuery listener
     */
    override fun setQuery(query: CharSequence?, submit: Boolean) {
        super.setQuery(query, false)
        // If submit is requested, force the query
        if (submit) listener?.onDebouncedQuery(this,query ?: "", true)
    }

    fun setOnDebouncedQueryListener(listener: ((view: View, queryText: CharSequence, isSubmitted: Boolean) -> Unit)?) {
        this.listener = object : SearchDebounceViewListener {
            override fun onDebouncedQuery(view: View, queryText: CharSequence, isSubmitted: Boolean) {
                listener?.invoke(view, queryText, isSubmitted)
            }
        }
    }
}