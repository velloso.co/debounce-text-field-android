package co.velloso.android.textfield

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit


class DebounceEditText @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = R.attr.editTextStyle

) : TextInputEditText(context, attrs, defStyleAttr) {

    var maxDebounce: Int = 2

    var timeDebounce: Long = 400L

    private var disposable: Disposable? = null

    private var debounceCounter = 0

    private var listener: SearchDebounceViewListener? = null

    interface SearchDebounceViewListener {
        fun onDebouncedQuery(queryText: CharSequence)
    }

    init {
        // Custom attributes
        val a = context.obtainStyledAttributes(attrs, R.styleable.DebounceEditText, defStyleAttr, 0)

        maxDebounce = a.getInteger(R.styleable.DebounceEditText_maxDebounce, 2)
        timeDebounce = a.getInteger(R.styleable.DebounceEditText_timeDebounce, 400).toLong()

        a.recycle()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        disposable = textChanges()
                .filter { input ->
                    debounceCounter += 1
                    // If we are cleaning the view or we already bounce MAX_DEBOUNCE times call the
                    // update immediately
                    if (input.isEmpty() || debounceCounter > maxDebounce) {
                        listener?.onDebouncedQuery(input.toString())
                        debounceCounter = 0
                        false
                    } else true
                }
                .debounce(timeDebounce, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe { queryEvent ->
                    // Debounce do not cancel a queued event if the previous filter return false,
                    // since it only cancel a queued event if .debounce() is called again. Example:
                    // Call is made to query "c" term and it enters the queue. If the user search
                    // another term "ca", .debounce() will be called again, removing "c" from the
                    // queue, replacing it with "ca" and resetting the timer. But if instead the user
                    // backspace the previous "c", the filter() function will return false, debounce
                    // will not be called and "c" will not leave the queue. So to avoid a second
                    // call to the updateProductResults we need a second check. We use the
                    // debounceCounter > 0 because every time the updateProductResults is called we set
                    // debounceCounter to zero.
                    if (debounceCounter > 0) {
                        // Update views that need result data with debounce delay
                        listener?.onDebouncedQuery(queryEvent.toString())
                        debounceCounter = 0
                    }
                }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        disposable?.dispose()
    }

    /**
     * Sets a query string in the text field and optionally submits the query as well.
     *
     * @param submit if true, immediately call onDebouncedQuery listener
     */
    fun submitQuery(query: CharSequence?, submit: Boolean) {
        setText(query)
        // If submit is requested, force the query
        if (submit) listener?.onDebouncedQuery(query ?: "")
    }

    fun setOnDebouncedQueryListener(listener: ((queryText: CharSequence) -> Unit)?) {
        this.listener = object : SearchDebounceViewListener {
            override fun onDebouncedQuery(queryText: CharSequence) {
                listener?.invoke(queryText)
            }
        }
    }
}